# https://gitlab.com/sgrigorov/ipxe/-/raw/master/files/create.kpxe.and.efi.gitlab.sh

sudo dnf install git openssl -y

git clone https://github.com/ipxe/ipxe.git

cd ipxe/src/

openssl s_client -showcerts -verify 5 -connect gitlab.com:443 < /dev/null | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > gitlab.certs.crt
csplit -k -f gitlab.crt.  gitlab.certs.crt '/END CERTIFICATE/+1' {1}


sed -i 's|#define PRODUCT_NAME ""|#define PRODUCT_NAME "Stan\x27s iPXE boot - created '"`date -u +"%b %d, %Y - %H:%M %Z"`"'"|' config/branding.h
sed -i 's|//#define[[:space:]]CONSOLE_VMWARE|#define       CONSOLE_VMWARE|' config/console.h

sed -i 's/undef[[:space:]]DOWNLOAD_PROTO_HTTPS/define DOWNLOAD_PROTO_HTTPS/' config/general.h
sed -i 's/undef[[:space:]]DOWNLOAD_PROTO_NFS/define DOWNLOAD_PROTO_NFS/' config/general.h
sed -i 's|#define[[:space:]]CRYPTO_80211_W|#undef CRYPTO_80211_W|' config/general.h

sed -i 's|//#define[[:space:]]VMWARE_SETTINGS|#define       VMWARE_SETTINGS|' config/settings.h

curl -k https://gitlab.com/sgrigorov/ipxe/-/raw/master/files/chain.gitlab.bootstrap.ipxe --output chain.gitlab.bootstrap.ipxe


sudo dnf install -y gcc binutils make perl xz xz-devel mtools mkisofs genisoimage syslinux


make bin/undionly.kpxe EMBED=chain.gitlab.bootstrap.ipxe CERT=gitlab.crt.00,gitlab.crt.01 TRUST=gitlab.crt.01,gitlab.crt.02
mv bin/undionly.kpxe ../../undionly.gitlab2.kpxe
make bin-x86_64-efi/ipxe.efi EMBED=chain.gitlab.bootstrap.ipxe CERT=gitlab.crt.00,gitlab.crt.01 TRUST=gitlab.crt.01,gitlab.crt.02
mv bin-x86_64-efi/ipxe.efi ../../ipxe.gitlab2.efi


# http://boot.ipxe.org/undionly.kpxe
# http://boot.ipxe.org/ipxe.efi

#   option client-arch code 93 = unsigned integer 16;
#   if option client-arch != 00:00 {
#      filename "ipxe.efi";
#   } else {
#      filename "undionly.kpxe";
#   }
#   next-server X.X.X.X;
