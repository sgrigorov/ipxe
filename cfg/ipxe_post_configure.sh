if [ -f "/etc/os-release" ]; then
    os_id=`grep \^ID= /etc/os-release | sed "s/ID=//"| sed "s/\"//g"`
    os_ver_id=`grep \^VERSION_ID= /etc/os-release | sed "s/VERSION_ID=//"| sed "s/\"//g"`
    os_pretty_name=`grep \^PRETTY_NAME= /etc/os-release | sed "s/PRETTY_NAME=//"| sed "s/\"//g"`
    echo "OS:"$os_id"<"
    case $os_id in
        debian | ubuntu | zorin | pop | raspbian)
            echo "OS - " $os_pretty_name
            echo "Version ID " $os_ver_id
            apt-get update && apt-get install -y ansible git
        ;;
        centos | almalinux | rocky)
            echo "OS - " $os_pretty_name
            echo "Version ID " $os_ver_id
            case ${os_ver_id:0:1} in
                8)
                    echo $os_id $os_ver_id
                    #echo -e "stan\tALL=(ALL)\tNOPASSWD:ALL" > /etc/sudoers.d/stan
                    #chmod 400 /etc/sudoers.d/stan
                    grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
                    dnf -y install  epel-release
                    dnf -y update
                    dnf -y install  ansible git #qemu-guest-agent
                    # implemet if virtual for qemu-guest-agent
                    # if ( dmidecode -s systemproduct-name == "KVM/QEMU") install qemu-guest-agent
                    # if ( dmidecode -s systemproduct-name == "Virtual Box") install ???????
                    # Not needed !!! if ( dmidecode -s systemproduct-name == "VMware Virtual Platform") install open-vm-tools
                ;;
                7)
                    echo $os_id $os_ver_id
                    #echo -e "stan\tALL=(ALL)\tNOPASSWD:ALL" > /etc/sudoers.d/stan
                    #chmod 400 /etc/sudoers.d/stan
                    yum -y install epel-release
                    yum -y update
                    yum -y install git ansible
                ;;
                *)
                    echo "Other CentOS"
                    #exit
                ;;
            esac
        ;;
        fedora)
            echo "OS - Fedora"
            echo "Version ID " $os_ver_id
            grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
            dnf -y install  epel-release
            dnf -y update
            dnf -y install  ansible git #qemu-guest-agent
            # implemet if virtual for qemu-guest-agent
        ;;
        ol)
            echo "OS - Oracle Linux"
            echo "Version ID " $os_ver_id
            grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
            # case ${os_ver_id:0:1} in
            #     8)
            #         echo $os_id $os_ver_id
            #         dnf -y install  oracle-epel-release-el8
            #     ;;
            #     9)
            #         echo $os_id $os_ver_id
            #         dnf -y install  oracle-epel-release-el9
            #     ;;
            # esac
            dnf -y install  oracle-epel-release-el${os_ver_id:0:1}
            dnf -y update
            dnf -y install  ansible git #qemu-guest-agent
            #dnf -y install  ansible-core git #qemu-guest-agent
            # implemet if virtual for qemu-guest-agent
        ;;
        rhel1)
            echo "OS - RedHat Enterprise Linux"
            echo "Version ID " $os_ver_id
            echo "[RHEL-BaseOS-Local-DVD]" > /etc/yum.repos.d/local.baseos.dvd.repo
            echo "name=RedHat Enterprise Linux 8 BaseOS Local DVD" >> /etc/yum.repos.d/local.baseos.dvd.repo
            echo "baseurl=http://192.168.0.34/mnt/rhel/BaseOS/" >> /etc/yum.repos.d/local.baseos.dvd.repo
            echo "gpgkey=http://192.168.0.34/mnt/rhel/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/local.baseos.dvd.repo
            echo "gpgcheck=1" >> /etc/yum.repos.d/local.baseos.dvd.repo
            echo "enabled=1"  >> /etc/yum.repos.d/local.baseos.dvd.repo
            echo "[RHEL-AppStream-Local-DVD]" > /etc/yum.repos.d/local.appstream.dvd.repo
            echo "name=RedHat Enterprise Linux 8 AppStream Local DVD" >> /etc/yum.repos.d/local.appstream.dvd.repo
            echo "baseurl=http://192.168.0.34/mnt/rhel/AppStream/" >> /etc/yum.repos.d/local.appstream.dvd.repo
            echo "gpgkey=http://192.168.0.34/mnt/rhel/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/local.appstream.dvd.repo
            echo "gpgcheck=1" >> /etc/yum.repos.d/local.appstream.dvd.repo
            echo "enabled=1"  >> /etc/yum.repos.d/local.appstream.dvd.repo
            dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
            dnf -y update
            dnf -y install  ansible git #qemu-guest-agent
            # implemet if virtual for qemu-guest-agent
        ;;
        manjaro)
            echo "OS - Manjaro Linux"
            echo "Version ID " $os_ver_id
        ;;
        *)
            echo "OS - Other"
            exit
        ;;
    esac
else
    exit
fi

ansible-pull -U https://gitlab.com/sgrigorov/masterless.git | tee /usr/share/ansible/ansible-first-run.log
