lang en_US
keyboard us
timezone America/Edmonton --isUtc
rootpw $6$NQZfT4B.sFKPhjiT$J5BXC2pDTqAS.9Rm6tDFUIjGs1l3GTpIvLpq41CQlB821npNhG/1aDExvFeXiIYklqvg9YpXPQF25Fc3hF6DP1 --iscrypted 
sshkey --username=root "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDJ5RmJUuwp5dDE+8tJ1bY/JZ3FCPT/M3cGQXhFfCQ8VxBeG4own/fWaV9JBOKk8QsFa8IiW3o3nDoSr9CKEmXvbneY3JXRaFeBTvmuoOMuK8+4CnFsI7GfcvhwHEvwlDmCXivpCq1ZCZ6Gn280NUpWqP5sovnVjruI0us/zd/awOYSVuG1+N+q/1gDjS6QxR9ARE90dG7Pxk/8H66FQRHkxwpgkg32ScduBsuNX9Xf7n4lVJWRs5v7xAmAIWn7i/EgTT0S75x49oJH6J1f3Xuyj9XAvERJWBRds3AE/rG+y1hwNBQ7pPz1FnJH9/ntYQ/Gt1sSQS6Xgpze4RVOcUddPH8L4In/uD/fs/ybjg+ZNculT+hUeJa2O53fJg3b67wq74a5WgTFXPNodf6EoYB9Opo4/FZ411p3ymiYZXy+5RN7b4C6jlFnMRosxpVZ221BrVDtyh5u3PAjaFc5PEKlFbETS6h1G1ph3OyNU1xT5ulQH0vhaLisVkigvGfwJgU="
user --name=stan --password=$6$ltMA/c5e2wyALIPc$ve8HfKCdzcuWzwX23YWDgbROSqUonMvunUGcwuGsrcjR9cezpqCRrRD5SmOwqlIR/TVSJ4JXpfj1kLeGjoMUO. --iscrypted --groups=wheel
sshkey --username=stan "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDs1mx/twPRW0BGska8CdDz+9bgvw1CD9SCgJymuGn1gA/EZvGCFdQDwkoPZbgKon0IscVsHABzakaIodRlRJrHH0n2gYCT1shFlpOtHv7ljeu8O8K5Lm2+2DtajBWgSCVTQ8P+y41UQLlvSs9f07tBbGyyQXxX4Pl1NB0yyJw5+qfWaSetqO/WV5SaCnvRklB52d6D+4K+/79wsR+bRd35GtRT6CO9p+X+1pxbTm0zVtSz8EgdS0aZOkXsseC/2d9NjS6ggj1j9vLWbSI6iYKM/gpMu3PFOWZg5sYVxUkk4TWj/aB/lD61YuiI1RvC2yqzkZjZq9NDVaTkOHXPkACM/nmSie/+48RGgzWFmvC7nTPsPHwnTuw6uYiFQGC4jeHT8u8Rcid2vAvIVRL5vTu3GWhq9HbwSbkmHb3HGHNfm0E6ufDWMfV5kZ9b7wLqTEW1kpwa/ua/jAnQdt9M2tVwOvj9wdag19USrwQdrrPOQHs6UnvYiL7WRb8wwLZ1BDE="
#platform x86, AMD64, or Intel EM64T
reboot
#text
#url --url=http://mirror.centos.org/centos/7/os/x86_64
#url --url=http://mirrors.kernel.org/fedora/releases/31/Server/x86_64/os
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
autopart
#auth --passalgo=sha512 --useshadow
selinux --enforcing
firewall --enabled --ssh
skipx
firstboot --disable
%pre
#if [ -f "/mnt/sysroot/etc/dnf/dnf.conf"]
#    grep -q ^fastestmirror /mnt/sysroot/etc/dnf/dnf.conf || echo fastestmirror=True >> /mnt/sysroot/etc/dnf/dnf.conf
#fi
%end
#%post
%post --log=/root/ks-post-install.log
curl -c -L https://gitlab.com/sgrigorov/ipxe/raw/master/cfg/ipxe_post_configure.sh | bash
%end
%packages
#@base
%end
